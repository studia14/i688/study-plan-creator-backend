package pl.ioi.studyplancreator.domain.application.room;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.room.GetRoomsQuery;
import pl.ioi.studyplancreator.domain.application.repository.RoomRepository;
import pl.ioi.studyplancreator.domain.types.room.Rooms;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@RequiredArgsConstructor
@Component
public class GetRoomQueryHandler implements QueryHandler<Rooms, GetRoomsQuery> {

    private final RoomRepository repository;

    @Override
    public Rooms handle(GetRoomsQuery query) {
        return Rooms.of(repository.getAll().getRooms());
    }
}
