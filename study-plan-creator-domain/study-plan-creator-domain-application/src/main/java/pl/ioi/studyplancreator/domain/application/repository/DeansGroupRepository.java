package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.DeansGroup;
import pl.ioi.studyplancreator.domain.types.DeansGroupDto;

import java.util.List;

public interface DeansGroupRepository {
    void save(DeansGroupDto deansGroup);
    void deleteById(Long id);
    DeansGroup findById(Long id);
    List<DeansGroup> findAll();
}
