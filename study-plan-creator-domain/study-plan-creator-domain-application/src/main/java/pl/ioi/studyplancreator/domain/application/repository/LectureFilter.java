package pl.ioi.studyplancreator.domain.application.repository;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LectureFilter {
    private String group;
    private String lecturer;
    private String classroom;
    private String year;
    private String phrase;
}
