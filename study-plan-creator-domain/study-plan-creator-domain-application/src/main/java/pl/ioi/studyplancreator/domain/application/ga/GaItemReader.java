package pl.ioi.studyplancreator.domain.application.ga;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.application.repository.*;
import pl.ioi.studyplancreator.domain.types.ga.*;

import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class GaItemReader implements Tasklet, StepExecutionListener {

    private final LecturesRepository lecturesRepository;
    private final LecturersRepository lecturersRepository;
    private final LectureTypesRepository lectureTypesRepository;
    private final RoomsRepository roomsRepository;
    private final DeansGroupRepository deansGroupRepository;
    private final TimeslotRepository timeslotRepository;
    private final GaMapper gaMapper;

    private Timetable timetable;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        lecturesRepository.deleteAll();
        val lecturers = lecturersRepository.findAll()
                .stream()
                .map(gaMapper::gaProffessor)
                .collect(Collectors.toMap(GaProfessor::getId, Function.identity()));
        val lectureTypes = lectureTypesRepository.findAll()
                .stream()
                .map(gaMapper::gaLectureType)
                .collect(Collectors.toMap(GaLectureType::getId, Function.identity()));
        val rooms = roomsRepository.findAll()
                .stream()
                .map(gaMapper::gaRoom)
                .collect(Collectors.toMap(GaRoom::getId, Function.identity()));
        val groups = deansGroupRepository.findAll()
                .stream()
                .map(gaMapper::gaGroup)
                .collect(Collectors.toMap(GaGroup::getId, Function.identity()));
        val timeslots = timeslotRepository.findAll()
                .stream()
                .map(gaMapper::gaTimeslot)
                .collect(Collectors.toMap(GaTimeslot::getId, Function.identity()));
        this.timetable = Timetable.builder()
                .proffesors(lecturers)
                .modules(lectureTypes)
                .rooms(rooms)
                .groups(groups)
                .timeslots(timeslots)
                .build();
        return RepeatStatus.FINISHED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        stepExecution
                .getJobExecution()
                .getExecutionContext()
                .put("timetable", this.timetable);
        return ExitStatus.COMPLETED;
    }
}
