package pl.ioi.studyplancreator.domain.application.semester;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.semester.AddNewSemesterCommand;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class AddSemesterCommandHandler implements CommandHandler<Void, AddNewSemesterCommand> {

    private final SemesterRepository repository;

    @Override
    public Void handle(AddNewSemesterCommand command) {
        repository.save(command.getDto());
        return null;
    }
}
