package pl.ioi.studyplancreator.domain.application.deansgroup;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.deansgroup.FindDeansGroupQuery;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.ioi.studyplancreator.domain.types.DeansGroupDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@Component
@RequiredArgsConstructor
public class FindDeansGroupByIdQueryHandler implements QueryHandler<DeansGroupDto, FindDeansGroupQuery> {

    private final DeansGroupRepository deansGroupRepository;

    @Override
    public DeansGroupDto handle(FindDeansGroupQuery query) {
        var deansGroup = deansGroupRepository.findById(query.getId());
        return DeansGroupDto.builder()
                .id(deansGroup.getId().toString())
                .name(deansGroup.getName())
                .semesterId(deansGroup.getSemester().getId())
                .semesterName(deansGroup.getSemester().getName())
                .build();
    }
}
