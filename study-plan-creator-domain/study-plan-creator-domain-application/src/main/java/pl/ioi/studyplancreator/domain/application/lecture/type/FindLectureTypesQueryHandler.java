package pl.ioi.studyplancreator.domain.application.lecture.type;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.type.FindLectureTypesQuery;
import pl.ioi.studyplancreator.domain.application.repository.LectureTypesRepository;
import pl.ioi.studyplancreator.domain.types.LectureTypesDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@RequiredArgsConstructor
@Component
@ToString
public class FindLectureTypesQueryHandler implements QueryHandler<LectureTypesDto, FindLectureTypesQuery> {

    private final LectureTypesRepository lectureTypesRepository;

    @Override
    public LectureTypesDto handle(FindLectureTypesQuery query) {
        return LectureTypesDto.of(lectureTypesRepository.findAll());
    }
}
