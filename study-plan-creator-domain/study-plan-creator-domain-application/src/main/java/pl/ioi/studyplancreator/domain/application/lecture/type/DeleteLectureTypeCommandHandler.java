package pl.ioi.studyplancreator.domain.application.lecture.type;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.type.DeleteLectureTypeCommand;
import pl.ioi.studyplancreator.domain.application.repository.LectureTypesRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class DeleteLectureTypeCommandHandler implements CommandHandler<Void, DeleteLectureTypeCommand> {

    private final LectureTypesRepository lectureTypesRepository;

    @Override
    public Void handle(DeleteLectureTypeCommand command) {
        lectureTypesRepository.deleteById(command.getId());
        return null;
    }
}
