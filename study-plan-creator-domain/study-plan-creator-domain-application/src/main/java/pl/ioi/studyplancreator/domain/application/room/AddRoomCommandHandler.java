package pl.ioi.studyplancreator.domain.application.room;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.room.AddNewRoomCommand;
import pl.ioi.studyplancreator.domain.application.repository.RoomRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class AddRoomCommandHandler implements CommandHandler<Void, AddNewRoomCommand> {

    private final RoomRepository roomRepository;

    @Override
    public Void handle(AddNewRoomCommand command) {
        roomRepository.save(command.getDto());
        return null;
    }
}
