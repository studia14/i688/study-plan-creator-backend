package pl.ioi.studyplancreator.domain.application.ga;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.types.*;
import pl.ioi.studyplancreator.domain.types.ga.*;

@Mapper(componentModel = "spring")
@Component
public interface GaMapper {

    GaProfessor gaProffessor(Lecturer input);

    @Mapping(target = "professors", source = "lecturers")
    GaLectureType gaLectureType(LectureType input);

    GaRoom gaRoom(Room input);

    GaRoomPurpose gaRoomPurpose(RoomPurpose input);

    @Mapping(target = "lectureTypes", source = "semester.lectureTypes")
    GaGroup gaGroup(DeansGroup input);

    GaTimeslot gaTimeslot(Timeslot input);
}
