package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.Semester;
import pl.ioi.studyplancreator.domain.types.SemesterDto;

import java.util.List;

public interface SemesterRepository {
    List<Semester> getSemesters();
    void save(SemesterDto dto);
    void update(SemesterDto dto, Long id);
    void deleteById(Long id);
}
