package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.LectureType;

import java.util.List;

public interface LectureTypesRepository {

    List<LectureType> findAll();
    void save(LectureType lectureType);
    void update(LectureType lectureType);
    void deleteById(Long id);
}
