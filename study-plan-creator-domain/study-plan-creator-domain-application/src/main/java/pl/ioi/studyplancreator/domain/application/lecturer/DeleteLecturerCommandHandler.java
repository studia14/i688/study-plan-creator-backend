package pl.ioi.studyplancreator.domain.application.lecturer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.DeleteLecturerCommand;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@Component
@RequiredArgsConstructor
public class DeleteLecturerCommandHandler implements CommandHandler<Void, DeleteLecturerCommand> {

    private final LecturersRepository lecturersRepository;

    @Override
    public Void handle(DeleteLecturerCommand command) {
        lecturersRepository.deleteById(command.getId());
        return null;
    }
}
