package pl.ioi.studyplancreator.domain.application.generator;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.generator.InitPlanGeneratorCommand;
import pl.ioi.studyplancreator.domain.application.repository.GeneratorJobLauncher;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@Component
@RequiredArgsConstructor
public class InitPlanGeneratorCommandHandler implements CommandHandler<Void, InitPlanGeneratorCommand> {

    private final GeneratorJobLauncher generatorJobLauncher;

    @Override
    public Void handle(InitPlanGeneratorCommand command) {
        generatorJobLauncher.start();
        return null;
    }
}
