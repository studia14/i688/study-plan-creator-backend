package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.Room;

import java.util.List;

public interface RoomsRepository {
    List<Room> findAll();
}
