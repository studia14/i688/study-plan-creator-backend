package pl.ioi.studyplancreator.domain.application.lecturer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.UpdateLecturerCommand;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class UpdateLecturerCommandHandler implements CommandHandler<Void, UpdateLecturerCommand> {

    private final LecturersRepository lecturersRepository;

    @Override
    public Void handle(UpdateLecturerCommand command) {
        lecturersRepository.update(command.getLecturerDto(), command.getId());
        return null;
    }
}
