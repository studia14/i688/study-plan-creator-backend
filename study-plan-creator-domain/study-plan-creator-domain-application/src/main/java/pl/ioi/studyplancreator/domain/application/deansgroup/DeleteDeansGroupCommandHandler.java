package pl.ioi.studyplancreator.domain.application.deansgroup;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.deansgroup.DeleteDeansGroupCommand;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class DeleteDeansGroupCommandHandler implements CommandHandler<Void, DeleteDeansGroupCommand> {

    private final DeansGroupRepository deansGroupRepository;

    @Override
    public Void handle(DeleteDeansGroupCommand command) {
        deansGroupRepository.deleteById(command.getId());
        return null;
    }
}
