package pl.ioi.studyplancreator.domain.application.deansgroup;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.deansgroup.FindDeansGroupsQuery;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.ioi.studyplancreator.domain.types.DeansGroupDto;
import pl.ioi.studyplancreator.domain.types.DeansGroups;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class FindDeansGroupsQueryHandler implements QueryHandler<DeansGroups, FindDeansGroupsQuery> {

    private final DeansGroupRepository deansGroupRepository;

    @Override
    public DeansGroups handle(FindDeansGroupsQuery query) {
        return DeansGroups.of(
                deansGroupRepository.findAll().stream()
                        .map(deansGroup -> DeansGroupDto.builder()
                                .id(deansGroup.getId().toString())
                                .name(deansGroup.getName())
                                .semesterId(deansGroup.getSemester().getId())
                                .semesterName(deansGroup.getSemester().getName())
                                .build())
                        .collect(Collectors.toList())
        );
    }
}
