package pl.ioi.studyplancreator.domain.application.repository;

public interface GeneratorJobLauncher {
    void start();
}
