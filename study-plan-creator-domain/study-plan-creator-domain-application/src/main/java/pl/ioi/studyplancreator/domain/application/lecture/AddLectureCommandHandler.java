package pl.ioi.studyplancreator.domain.application.lecture;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.AddLectureCommand;
import pl.ioi.studyplancreator.domain.application.repository.LecturesRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class AddLectureCommandHandler implements CommandHandler<Void, AddLectureCommand> {

    private final LecturesRepository lecturesRepository;

    @Override
    public Void handle(AddLectureCommand command){
        lecturesRepository.save(command.getLecture());
        return null;
    }
}
