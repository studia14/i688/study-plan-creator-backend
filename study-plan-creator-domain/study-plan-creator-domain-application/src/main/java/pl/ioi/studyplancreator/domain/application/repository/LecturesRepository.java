package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.application.ga.FullLecture;
import pl.ioi.studyplancreator.domain.types.Lecture;
import pl.ioi.studyplancreator.domain.types.LectureDto;
import pl.ioi.studyplancreator.domain.types.LecturerDto;

import java.util.List;

public interface LecturesRepository {
    List<Lecture> findLecturesUsingFilter(LectureFilter lecturerFilter);
    void save(LectureDto lectureDto);
    void save(List<FullLecture> lectures);
    void update(LectureDto lectureDto, Long id);
    void deleteById(Long id);
    void deleteAll();
}
