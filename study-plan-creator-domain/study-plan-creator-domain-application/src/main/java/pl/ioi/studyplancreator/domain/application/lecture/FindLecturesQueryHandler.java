package pl.ioi.studyplancreator.domain.application.lecture;

import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.FindLecturesQuery;
import pl.ioi.studyplancreator.domain.application.repository.LecturesRepository;
import pl.ioi.studyplancreator.domain.types.lecture.Lectures;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@Component
@RequiredArgsConstructor
public class FindLecturesQueryHandler implements QueryHandler<Lectures, FindLecturesQuery> {

    private final LecturesRepository lecturerRepository;
    private final LecturesMaper lecturesMaper;

    @Override
    public Lectures handle(FindLecturesQuery query) {
        return Lectures.of(lecturerRepository.findLecturesUsingFilter(
                lecturesMaper.findLecturesQueryToLectureFilter(query)
        ));
    }
}
