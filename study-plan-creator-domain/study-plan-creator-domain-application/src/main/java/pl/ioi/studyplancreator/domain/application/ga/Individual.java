package pl.ioi.studyplancreator.domain.application.ga;

import lombok.Getter;
import lombok.Setter;
import pl.ioi.studyplancreator.domain.types.ga.GaGroup;

public class Individual {

    @Getter
    private long[] chromosome;

    @Getter
    @Setter
    private double fitness = -1;

    @Getter
    @Setter
    private double roomFitness = -1;

    public Individual(Timetable timetable) {
        int numClasses = timetable.getNumClasses();

        int chromosomeLength = numClasses * 3;
        long newChromosome[] = new long[chromosomeLength];
        int chromosomeIndex = 0;

        for (GaGroup group: timetable.getGroupsCollection()) {
            for (Long moduleId: group.getModuleIds()) {

                long timeslotId = timetable.getRandomTimeslot().getId();
                newChromosome[chromosomeIndex] = timeslotId;
                chromosomeIndex++;

                var availableRoomTypes = timetable.getModules().get(moduleId).getNeededRoomTypes();
                long roomID = timetable.getRandomRoom(availableRoomTypes).getId();
                newChromosome[chromosomeIndex] = roomID;
                chromosomeIndex++;

                long proffessorId = timetable.getModules().get(moduleId).randomProfessor().getId();
                newChromosome[chromosomeIndex] = proffessorId;
                chromosomeIndex++;
            }
        }
        this.chromosome = newChromosome;
    }

    public Individual(int chromosomeLength) {
        long[] individual = new long[chromosomeLength];
        for (int gene = 0; gene < chromosomeLength; gene++) {
            individual[gene] = gene;
        }

        this.chromosome = individual;
    }

    public int getChromosomeLength() {
        return this.chromosome.length;
    }

    public void setGene(int offset, long gene) {
        this.chromosome[offset] = gene;
    }
    public long getGene(int offset) {
        return this.chromosome[offset];
    }


}
