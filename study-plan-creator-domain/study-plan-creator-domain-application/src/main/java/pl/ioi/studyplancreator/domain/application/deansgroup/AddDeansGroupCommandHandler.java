package pl.ioi.studyplancreator.domain.application.deansgroup;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.ioi.studyplancreator.domain.api.deansgroup.AddDeansGroupCommand;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class AddDeansGroupCommandHandler implements CommandHandler<Void, AddDeansGroupCommand> {

    private final DeansGroupRepository deansGroupRepository;

    @Override
    @Transactional
    public Void handle(AddDeansGroupCommand command) {
        deansGroupRepository.save(command.getDeansGroup());
        return null;
    }
}
