package pl.ioi.studyplancreator.domain.application.ga;


import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Population {

    @Getter
    private Individual[] individuals;

    @Setter
    private double populationFitness = -1;

    public Population(int populationSize) {
        this.individuals = new Individual[populationSize];
    }

    public Population(int populationSize, Timetable timetable) {
        this.individuals = new Individual[populationSize];

        for(int individualCount = 0; individualCount < populationSize; individualCount++) {
            var individual = new Individual(timetable);
            this.individuals[individualCount] = individual;
        }
    }

    public Individual getFittest(int offset) {
        // Order population by fitness
        Arrays.sort(this.individuals, new Comparator<Individual>() {
            @Override
            public int compare(Individual o1, Individual o2) {
                if (o1.getFitness() > o2.getFitness()) {
                    return -1;
                } else if (o1.getFitness() < o2.getFitness()) {
                    return 1;
                }
                else if (o1.getRoomFitness() > o2.getRoomFitness()) {
                    return -1;
                } else if (o1.getRoomFitness() < o2.getRoomFitness()) {
                    return 1;
                }
                return 0;
            }
        });

        // Return the fittest individual
        return this.individuals[offset];
    }

    public int size() {
        return this.individuals.length;
    }

    public Individual getIndividual(int offset) {
        return individuals[offset];
    }

    public Individual setIndividual(int offset, Individual individual) {
        return individuals[offset] = individual;
    }

    public void shuffle() {
        Random rnd = new Random();
        for (int i = individuals.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            Individual a = individuals[index];
            individuals[index] = individuals[i];
            individuals[i] = a;
        }
    }
}
