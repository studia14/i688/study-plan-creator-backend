package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.RoomDto;
import pl.ioi.studyplancreator.domain.types.room.Rooms;

public interface RoomRepository {
    Rooms getAll();
    void save(RoomDto dto);
    void update(RoomDto dto, Long id);
    void deleteById(Long id);
}
