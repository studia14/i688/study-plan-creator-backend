package pl.ioi.studyplancreator.domain.application.ga;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import pl.ioi.studyplancreator.domain.types.*;

@Getter
@ToString
public class Lecture {
    private long id;
    private long groupId;
    private long moduleId;
    private long professorId;
    private long timeslotId;
    private long roomId;

    public Lecture(long classId, long groupId, long moduleId){
        this.id = classId;
        this.moduleId = moduleId;
        this.groupId = groupId;
    }

    public boolean equalsLecture(Lecture lecture) {
        if (lecture.getProfessorId() == professorId
        && lecture.getModuleId() == moduleId
        && lecture.getTimeslotId() == timeslotId
        && lecture.getRoomId() == roomId) {
            return true;
        }
        return false;
    }

    public void addProfessor(long professorId){
        this.professorId = professorId;
    }
    public void addTimeslot(long timeslotId){
        this.timeslotId = timeslotId;
    }
    public void setRoomId(long roomId){
        this.roomId = roomId;
    }

}
