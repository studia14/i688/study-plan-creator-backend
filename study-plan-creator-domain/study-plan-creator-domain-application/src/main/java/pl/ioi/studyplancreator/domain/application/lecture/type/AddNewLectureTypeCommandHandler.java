package pl.ioi.studyplancreator.domain.application.lecture.type;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.type.AddNewLectureTypeCommand;
import pl.ioi.studyplancreator.domain.application.repository.LectureTypesRepository;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@Component
@RequiredArgsConstructor
public class AddNewLectureTypeCommandHandler implements CommandHandler<Void, AddNewLectureTypeCommand> {

    private final LectureTypesRepository lectureTypesRepository;

    @Override
    public Void handle(AddNewLectureTypeCommand command) {
        lectureTypesRepository.save(LectureType.builder()
                .name(command.getName())
                .build());
        return null;
    }
}
