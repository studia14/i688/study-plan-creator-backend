package pl.ioi.studyplancreator.domain.application.ga;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
@Builder
public class LectureDefinition {
    private Long moduleId;
    private long professorId;
    private long timeslotId;
    private long roomId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LectureDefinition that = (LectureDefinition) o;
        return professorId == that.professorId && timeslotId == that.timeslotId && roomId == that.roomId && Objects.equals(moduleId, that.moduleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moduleId, professorId, timeslotId, roomId);
    }
}
