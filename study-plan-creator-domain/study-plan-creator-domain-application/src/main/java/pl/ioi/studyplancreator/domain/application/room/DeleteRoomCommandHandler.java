package pl.ioi.studyplancreator.domain.application.room;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.DeleteLecturerCommand;
import pl.ioi.studyplancreator.domain.api.room.DeleteRoomCommand;
import pl.ioi.studyplancreator.domain.application.repository.RoomRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class DeleteRoomCommandHandler implements CommandHandler<Void, DeleteRoomCommand> {

    private final RoomRepository repository;

    @Override
    public Void handle(DeleteRoomCommand command) {
        repository.deleteById(command.getId());
        return null;
    }
}
