package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.Lecturer;
import pl.ioi.studyplancreator.domain.types.LecturerDto;

import java.util.List;
import java.util.Optional;

public interface LecturersRepository {
    List<Lecturer> findAll();
    Optional<Lecturer> getLecturerById(Long id);
    void save(LecturerDto lecturerDto);
    void update(LecturerDto lecturerDto, Long id);
    void deleteById(Long id);
}
