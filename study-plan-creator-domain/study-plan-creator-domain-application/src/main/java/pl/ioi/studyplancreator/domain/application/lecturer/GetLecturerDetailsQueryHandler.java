package pl.ioi.studyplancreator.domain.application.lecturer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.GetLecturerDetailsQuery;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.ioi.studyplancreator.domain.types.Lecturer;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@Component
@RequiredArgsConstructor
public class GetLecturerDetailsQueryHandler implements QueryHandler<Lecturer, GetLecturerDetailsQuery> {

    private final LecturersRepository lecturersRepository;

    @Override
    public Lecturer handle(GetLecturerDetailsQuery query) {
        return lecturersRepository.getLecturerById(query.getId())
                .orElseThrow(); //TODO: Obsluga wyjątków i ładne wyświetlanie błędów
    }
}
