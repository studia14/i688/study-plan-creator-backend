package pl.ioi.studyplancreator.domain.application.deansgroup;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.ioi.studyplancreator.domain.api.deansgroup.UpdateDeansGroupCommand;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class UpdateDeansGroupCommandHandler implements CommandHandler<Void, UpdateDeansGroupCommand> {

    private final DeansGroupRepository deansGroupRepository;

    @Override
    @Transactional
    public Void handle(UpdateDeansGroupCommand command) {
        deansGroupRepository.save(command.getDeansGroup());
        return null;
    }
}
