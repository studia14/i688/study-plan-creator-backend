package pl.ioi.studyplancreator.domain.application.semester;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.semester.UpdateSemesterCommand;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class UpdateSemesterCommandHandler implements CommandHandler<Void, UpdateSemesterCommand> {

    private final SemesterRepository repository;

    @Override
    public Void handle(UpdateSemesterCommand command) {
        repository.update(command.getDto(), command.getId());
        return null;
    }
}
