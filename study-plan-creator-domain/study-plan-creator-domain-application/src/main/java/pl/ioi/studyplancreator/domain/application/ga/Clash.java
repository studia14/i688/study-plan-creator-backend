package pl.ioi.studyplancreator.domain.application.ga;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor(staticName = "of")
@Getter
@ToString
public class Clash {
    private int count;
    private long notFilledRoomsCount;
}
