package pl.ioi.studyplancreator.domain.application.lecture;

import org.mapstruct.Mapper;
import pl.ioi.studyplancreator.domain.api.lecture.FindLecturesQuery;
import pl.ioi.studyplancreator.domain.application.repository.LectureFilter;
import pl.ioi.studyplancreator.domain.types.lecture.Lectures;

@Mapper(componentModel = "spring")
public interface LecturesMaper {

    LectureFilter findLecturesQueryToLectureFilter(FindLecturesQuery findLecturesQuery);
}
