package pl.ioi.studyplancreator.domain.application.ga;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class GaItemProcessor implements Tasklet, StepExecutionListener {

    private Timetable timetable;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        var ga = new GeneticAlgorithm(600, 0.01, 0.98, 1, 300);
        var population = ga.initPopulation(timetable);
        ga.evalPopulation(population, timetable);
        int generation = 1;
        while (!ga.isTerminationConditionMet(generation, 1000)
        && !ga.isTerminationConditionMet(population)) {
            log.info("G {} Best fitness: {} {}", generation, population.getFittest(0).getFitness(), population.getFittest(0).getRoomFitness());
            population = ga.crossoverPopulation(population);
            population = ga.mutatePopulation(population, timetable);
            ga.evalPopulation(population, timetable);
            generation++;
        }
        timetable.createClasses(population.getFittest(0));
        log.info("Final solution fitness: " + population.getFittest(0).getFitness());
        log.info("Clashes: " + timetable.calcClashes(true));
        for (Lecture lecture : timetable.getLectures()) {
            log.info(lecture.toString());
        }
        return RepeatStatus.FINISHED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        ExecutionContext executionContext = stepExecution
                .getJobExecution()
                .getExecutionContext();
        this.timetable = (Timetable) executionContext.get("timetable");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        stepExecution
                .getJobExecution()
                .getExecutionContext()
                .put("lectures", groupLectures(timetable.getLectures()));
        return null;
    }

    private List<FullLecture> groupLectures(Lecture[] lectureList) {
        Map<LectureDefinition, List<Lecture>> groupedLectures = Arrays.stream(lectureList)
                .collect(Collectors.groupingBy(entry ->
                    LectureDefinition.builder()
                            .moduleId(entry.getModuleId())
                            .professorId(entry.getProfessorId())
                            .roomId(entry.getRoomId())
                            .timeslotId(entry.getTimeslotId())
                            .build()
                ));
        return groupedLectures.entrySet().stream()
                .map(entry -> FullLecture.builder()
                        .groupIds(entry.getValue().stream().map(Lecture::getGroupId).collect(Collectors.toList()))
                        .professorId(entry.getKey().getProfessorId())
                        .roomId(entry.getKey().getRoomId())
                        .moduleId(entry.getKey().getModuleId())
                        .timeslotId(entry.getKey().getTimeslotId())
                        .build())
                .collect(Collectors.toList());
    }
}
