package pl.ioi.studyplancreator.domain.application.lecture.type;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecture.type.UpdateLectureTypeCommand;
import pl.ioi.studyplancreator.domain.application.repository.LectureTypesRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class UpdateLectureTypeCommandHandler implements CommandHandler<Void, UpdateLectureTypeCommand> {

    private final LectureTypesRepository lectureTypesRepository;

    @Override
    public Void handle(UpdateLectureTypeCommand command) {
        lectureTypesRepository.update(command.getLectureType());
        return null;
    }
}
