package pl.ioi.studyplancreator.domain.application.semester;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.semester.GetSemestersQuery;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.ioi.studyplancreator.domain.types.Semester;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

import java.util.List;

@RequiredArgsConstructor
@Component
public class GetSemestersQueryHandler implements QueryHandler<List<Semester>, GetSemestersQuery> {

    private final SemesterRepository repository;

    @Override
    public List<Semester> handle(GetSemestersQuery query) {
        return repository.getSemesters();
    }
}
