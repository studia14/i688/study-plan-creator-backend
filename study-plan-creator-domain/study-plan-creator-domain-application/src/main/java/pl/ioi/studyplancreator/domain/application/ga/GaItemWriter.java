package pl.ioi.studyplancreator.domain.application.ga;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.ioi.studyplancreator.domain.application.repository.LecturesRepository;
import pl.ioi.studyplancreator.domain.types.LectureDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class GaItemWriter implements Tasklet, StepExecutionListener {

    private List<FullLecture> lectures;
    private final LecturesRepository lecturesRepository;

    @Override
    @Transactional
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        lecturesRepository.save(lectures);
        return RepeatStatus.FINISHED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        ExecutionContext executionContext = stepExecution
                .getJobExecution()
                .getExecutionContext();
        this.lectures = (List<FullLecture>) executionContext.get("lectures");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }
}
