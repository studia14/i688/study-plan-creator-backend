package pl.ioi.studyplancreator.domain.application.semester;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.semester.DeleteSemesterCommand;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class DeleteSemesterCommandHandler implements CommandHandler<Void, DeleteSemesterCommand> {

    private final SemesterRepository repository;

    @Override
    public Void handle(DeleteSemesterCommand deleteSemesterCommand) {
        repository.deleteById(deleteSemesterCommand.getId());
        return null;
    }
}
