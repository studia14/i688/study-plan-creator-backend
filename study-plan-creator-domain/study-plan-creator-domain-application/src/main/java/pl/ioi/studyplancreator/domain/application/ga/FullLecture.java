package pl.ioi.studyplancreator.domain.application.ga;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class FullLecture {
    private Long moduleId;
    private long professorId;
    private long timeslotId;
    private long roomId;
    private List<Long> groupIds;
}
