package pl.ioi.studyplancreator.domain.application.lecturer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.FindLecturersQuery;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.ioi.studyplancreator.domain.types.lecturer.Lecturers;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

@Component
@RequiredArgsConstructor
public class FindLecturersQueryHandler implements QueryHandler<Lecturers, FindLecturersQuery> {

    private final LecturersRepository lecturersRepository;

    @Override
    public Lecturers handle(FindLecturersQuery query) {
        return Lecturers.of(lecturersRepository.findAll());
    }
}
