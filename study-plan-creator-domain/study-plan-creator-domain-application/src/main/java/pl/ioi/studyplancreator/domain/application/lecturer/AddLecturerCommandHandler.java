package pl.ioi.studyplancreator.domain.application.lecturer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.AddLecturerCommand;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class AddLecturerCommandHandler implements CommandHandler<Void, AddLecturerCommand> {

    private final LecturersRepository lecturersRepository;

    @Override
    public Void handle(AddLecturerCommand command) {
        lecturersRepository.save(command.getLecturer());
        return null;
    }
}
