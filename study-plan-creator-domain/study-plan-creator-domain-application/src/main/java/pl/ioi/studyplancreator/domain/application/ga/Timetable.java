package pl.ioi.studyplancreator.domain.application.ga;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import pl.ioi.studyplancreator.domain.types.ga.*;

import java.io.Serializable;
import java.util.*;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Timetable implements Serializable {
    private Map<Long, GaProfessor> proffesors;
    private Map<Long, GaLectureType> modules;
    private Map<Long, GaRoom> rooms;
    private Map<Long, GaGroup> groups;
    private Map<Long, GaTimeslot> timeslots;
    private Lecture[] lectures;

    private int numClasses = 0;

    public Collection<GaGroup> getGroupsCollection() {
        return groups.values();
    }

    public GaTimeslot getRandomTimeslot() {
        Object[] timeslotArray = this.timeslots.values().toArray();
        return (GaTimeslot) timeslotArray[(int) (timeslotArray.length * Math.random())];
    }

    public Timetable(Timetable cloneable) {
        this.rooms = cloneable.getRooms();
        this.proffesors = cloneable.getProffesors();
        this.modules = cloneable.getModules();
        this.groups = cloneable.getGroups();
        this.timeslots = cloneable.getTimeslots();
    }

    public GaRoom getRandomRoom(List<GaRoomPurpose> correctRoomPurposes) {
        Object[] values = rooms.values().stream()
                .filter(room -> room.getPurposes().stream().anyMatch(correctRoomPurposes::contains)).toArray();
        return (GaRoom) values[(int) (values.length * Math.random())];
    }

    public int getNumClasses() {
        if (this.numClasses > 0) {
            return this.numClasses;
        }

        int numClasses = 0;
        GaGroup[] groups = this.groups.values().toArray(new GaGroup[this.groups.size()]);
        for (GaGroup group : groups) {
            numClasses += group.getModuleIds().size();
        }
        this.numClasses = numClasses;

        return this.numClasses;
    }

    public void createClasses(Individual individual) {
        Lecture lectures[] = new Lecture[this.getNumClasses()];
        long chromosome[] = individual.getChromosome();
        int chromosomePos = 0;
        int classIndex = 0;

        for (GaGroup group : this.getGroupsCollection()) {
            var moduleIds = group.getModuleIds();
            for (long moduleId : moduleIds) {
                lectures[classIndex] = new Lecture(classIndex, group.getId(), moduleId);

                // Add timeslot
                lectures[classIndex].addTimeslot(chromosome[chromosomePos]);
                chromosomePos++;

                // Add room
                lectures[classIndex].setRoomId(chromosome[chromosomePos]);
                chromosomePos++;

                // Add professor
                lectures[classIndex].addProfessor(chromosome[chromosomePos]);
                chromosomePos++;

                classIndex++;
            }
        }

        this.lectures = lectures;
    }

    public Clash calcClashes(boolean print) {
        int clashes = 0;
        Map<RoomTimeSlotPair, Long> roomTimeSlotPairLongMap = new HashMap<>();

        for (Lecture classA: this.lectures) {
            long actualCapacity = roomTimeSlotPairLongMap.getOrDefault(RoomTimeSlotPair.of(classA.getRoomId(), classA.getTimeslotId()), 0L);
            roomTimeSlotPairLongMap.put(RoomTimeSlotPair.of(classA.getRoomId(), classA.getTimeslotId()), actualCapacity + 1);
        }

        for (Lecture classA : this.lectures) {
            // Check if room is taken
            for (Lecture classB : this.lectures) {
                if (classA.getRoomId() == classB.getRoomId() && classA.getTimeslotId() == classB.getTimeslotId()
                        && classA.getId() != classB.getId()) {
                    if (classA.getProfessorId() != classB.getProfessorId() && classA.getModuleId() != classB.getModuleId()) {
                        clashes++;
                        if(print) {
                            log.info("Room clash: {} {} ", classA, classB);
                        }
                    }
                }
            }

            // Check if professor is available
            for (Lecture classB : this.lectures) {
                if (classA.getProfessorId() == classB.getProfessorId() && classA.getTimeslotId() == classB.getTimeslotId()
                        && classA.getId() != classB.getId()) {
                    if (classA.getRoomId() != classB.getRoomId() || classA.getModuleId() != classB.getModuleId()) {
                        clashes++;
                        if(print) {
                            log.info("Professor clash: {} {} ", classA, classB);
                        }
                    }

                }
            }
            // Check if timeslot is available
            for (Lecture classB : this.lectures) {
                if (classA.getGroupId() == classB.getGroupId() && classA.getModuleId() != classB.getModuleId()
                && classA.getRoomId() != classB.getRoomId() &&
                        classA.getProfessorId() != classB.getProfessorId() && classA.getTimeslotId() == classB.getTimeslotId()
                        && classA.getId() != classB.getId()) {
                    clashes++;
                    if(print) {
                        log.info("timeslot clash: {} {} ", classA, classB);
                    }
                }
            }
        }
        var notFilledRooms = roomTimeSlotPairLongMap.entrySet().stream()
                .filter(entry -> rooms.get(entry.getKey().getRoomId()).getCapacity() > entry.getValue())
                .count();
        var overloadedRooms = roomTimeSlotPairLongMap.entrySet().stream()
                .filter(entry ->  rooms.get(entry.getKey().getRoomId()).getCapacity() < entry.getValue())
                .count();
        if(print && overloadedRooms > 0) {
            roomTimeSlotPairLongMap.entrySet().stream()
                    .filter(entry ->  rooms.get(entry.getKey().getRoomId()).getCapacity() < entry.getValue())
                    .forEach(room -> {
                        log.info("Room capacity clash: {}", room.getKey());
                    });
        }
        return Clash.of(clashes + (int)overloadedRooms, notFilledRooms);
    }

    @Getter
    @EqualsAndHashCode
    @ToString
    @AllArgsConstructor(staticName = "of")
    private static class RoomTimeSlotPair {
        private long roomId;
        private long timeslotId;
    }
}
