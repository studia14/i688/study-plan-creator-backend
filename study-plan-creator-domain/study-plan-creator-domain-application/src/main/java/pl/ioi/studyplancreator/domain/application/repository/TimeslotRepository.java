package pl.ioi.studyplancreator.domain.application.repository;

import pl.ioi.studyplancreator.domain.types.Timeslot;

import java.util.List;

public interface TimeslotRepository {
    List<Timeslot> findAll();
}
