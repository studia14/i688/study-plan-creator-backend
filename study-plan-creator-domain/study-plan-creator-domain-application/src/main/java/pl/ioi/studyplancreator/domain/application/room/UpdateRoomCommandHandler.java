package pl.ioi.studyplancreator.domain.application.room;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.room.UpdateRoomCommand;
import pl.ioi.studyplancreator.domain.application.repository.RoomRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@RequiredArgsConstructor
@Component
public class UpdateRoomCommandHandler implements CommandHandler<Void, UpdateRoomCommand> {

    private final RoomRepository roomRepository;

    @Override
    public Void handle(UpdateRoomCommand command) {
        roomRepository.update(command.getRoom(), command.getId());
        return null;
    }
}
