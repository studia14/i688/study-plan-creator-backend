package pl.ioi.studyplancreator.domain.application.lecture;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.api.lecturer.DeleteLecturerCommand;
import pl.ioi.studyplancreator.domain.application.repository.LecturesRepository;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

@Component
@RequiredArgsConstructor
public class DeleteLectureCommandHandler implements CommandHandler<Void, DeleteLecturerCommand> {

    private final LecturesRepository lecturesRepository;

    @Override
    public Void handle(DeleteLecturerCommand command) {
        lecturesRepository.deleteById(command.getId());
        return null;
    }
}
