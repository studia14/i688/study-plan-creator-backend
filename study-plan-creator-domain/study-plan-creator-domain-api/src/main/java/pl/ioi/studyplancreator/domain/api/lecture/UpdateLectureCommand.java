package pl.ioi.studyplancreator.domain.api.lecture;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.LectureDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Setter
@ToString
public class UpdateLectureCommand implements Command<Void> {
    private Long id;
    private LectureDto dto;
}
