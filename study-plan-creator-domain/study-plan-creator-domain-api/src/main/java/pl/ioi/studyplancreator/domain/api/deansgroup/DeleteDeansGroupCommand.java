package pl.ioi.studyplancreator.domain.api.deansgroup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor(staticName = "of")
@Getter
public class DeleteDeansGroupCommand implements Command<Void> {
    private Long id;
}
