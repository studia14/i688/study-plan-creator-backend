package pl.ioi.studyplancreator.domain.api.room;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.room.Rooms;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

@NoArgsConstructor
@Getter
public class GetRoomsQuery implements Query<Rooms> {
}
