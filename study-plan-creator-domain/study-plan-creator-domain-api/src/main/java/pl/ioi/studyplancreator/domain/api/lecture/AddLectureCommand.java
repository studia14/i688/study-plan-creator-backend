package pl.ioi.studyplancreator.domain.api.lecture;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.LectureDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Setter
@ToString

public class AddLectureCommand implements Command<Void> {
    private LectureDto lecture;
}
