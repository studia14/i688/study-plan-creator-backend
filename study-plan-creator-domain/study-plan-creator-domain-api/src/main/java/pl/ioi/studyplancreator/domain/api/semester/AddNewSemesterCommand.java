package pl.ioi.studyplancreator.domain.api.semester;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.SemesterDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
@ToString
public class AddNewSemesterCommand implements Command<Void> {
    private SemesterDto dto;
}
