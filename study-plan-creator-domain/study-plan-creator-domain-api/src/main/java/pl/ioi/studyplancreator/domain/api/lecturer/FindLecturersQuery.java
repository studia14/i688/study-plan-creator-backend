package pl.ioi.studyplancreator.domain.api.lecturer;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.lecturer.Lecturers;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class FindLecturersQuery implements Query<Lecturers> {
}
