package pl.ioi.studyplancreator.domain.api.lecture.type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor(staticName = "of")
@Getter
@ToString
public class DeleteLectureTypeCommand implements Command<Void> {
    private Long id;
}
