package pl.ioi.studyplancreator.domain.api.room;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Setter
public class DeleteRoomCommand implements Command<Void> {
    private Long id;
}
