package pl.ioi.studyplancreator.domain.api.semester;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor(staticName = "of")
@Getter
@ToString
public class DeleteSemesterCommand implements Command<Void> {
    private Long id;
}
