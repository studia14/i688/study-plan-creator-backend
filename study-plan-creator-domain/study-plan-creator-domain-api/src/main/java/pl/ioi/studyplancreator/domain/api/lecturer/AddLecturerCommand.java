package pl.ioi.studyplancreator.domain.api.lecturer;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.LecturerDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Setter
@ToString
public class AddLecturerCommand implements Command<Void> {
    private LecturerDto lecturer;
}
