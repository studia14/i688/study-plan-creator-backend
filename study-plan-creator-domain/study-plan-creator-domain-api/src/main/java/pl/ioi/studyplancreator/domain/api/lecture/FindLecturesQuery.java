package pl.ioi.studyplancreator.domain.api.lecture;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.ioi.studyplancreator.domain.types.lecture.Lectures;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

@Getter
@AllArgsConstructor(staticName = "of")
@Setter
public class FindLecturesQuery implements Query<Lectures> {
    private String group;
    private String lecturer;
    private String classroom;
    private String year;
    private String phrase;
}
