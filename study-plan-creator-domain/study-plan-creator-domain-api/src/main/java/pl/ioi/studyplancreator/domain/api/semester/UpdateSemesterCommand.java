package pl.ioi.studyplancreator.domain.api.semester;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.SemesterDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Builder(toBuilder = true)
@Setter
@ToString
public class UpdateSemesterCommand implements Command<Void> {
    private Long id;
    private SemesterDto dto;
}
