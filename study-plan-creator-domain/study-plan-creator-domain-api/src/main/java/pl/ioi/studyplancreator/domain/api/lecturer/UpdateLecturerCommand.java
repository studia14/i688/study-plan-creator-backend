package pl.ioi.studyplancreator.domain.api.lecturer;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.LecturerDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Setter
@ToString
public class UpdateLecturerCommand implements Command<Void> {
    private Long id;
    private LecturerDto lecturerDto;
}
