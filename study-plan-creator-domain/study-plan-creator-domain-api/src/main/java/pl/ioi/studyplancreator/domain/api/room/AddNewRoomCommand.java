package pl.ioi.studyplancreator.domain.api.room;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.RoomDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Builder(toBuilder = true)
@Data
public class AddNewRoomCommand implements Command<Void> {
    private RoomDto dto;
}
