package pl.ioi.studyplancreator.domain.api.lecture.type;

import lombok.*;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
@ToString
public class AddNewLectureTypeCommand implements Command<Void> {
    private String name;
}
