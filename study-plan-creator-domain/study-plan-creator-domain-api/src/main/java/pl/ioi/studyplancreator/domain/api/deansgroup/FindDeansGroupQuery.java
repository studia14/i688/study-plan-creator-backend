package pl.ioi.studyplancreator.domain.api.deansgroup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.ioi.studyplancreator.domain.types.DeansGroupDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;


@AllArgsConstructor(staticName = "of")
@Getter
public class FindDeansGroupQuery implements Query<DeansGroupDto> {
    private Long id;
}
