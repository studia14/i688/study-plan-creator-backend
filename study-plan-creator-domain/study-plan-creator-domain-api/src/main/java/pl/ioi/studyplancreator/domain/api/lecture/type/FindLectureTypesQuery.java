package pl.ioi.studyplancreator.domain.api.lecture.type;

import lombok.NoArgsConstructor;
import pl.ioi.studyplancreator.domain.types.LectureTypesDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

@NoArgsConstructor
public class FindLectureTypesQuery implements Query<LectureTypesDto> {
}
