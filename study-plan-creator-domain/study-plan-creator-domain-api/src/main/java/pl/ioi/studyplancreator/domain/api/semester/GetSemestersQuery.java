package pl.ioi.studyplancreator.domain.api.semester;

import lombok.NoArgsConstructor;
import pl.ioi.studyplancreator.domain.types.Semester;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

import java.util.List;

@NoArgsConstructor
public class GetSemestersQuery implements Query<List<Semester>> {
}
