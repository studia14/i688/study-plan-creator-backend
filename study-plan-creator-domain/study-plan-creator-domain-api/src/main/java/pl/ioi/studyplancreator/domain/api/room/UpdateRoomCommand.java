package pl.ioi.studyplancreator.domain.api.room;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.RoomDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Builder(toBuilder = true)
@Setter
@ToString
public class UpdateRoomCommand implements Command<Void> {
    private Long id;
    private RoomDto room;
}
