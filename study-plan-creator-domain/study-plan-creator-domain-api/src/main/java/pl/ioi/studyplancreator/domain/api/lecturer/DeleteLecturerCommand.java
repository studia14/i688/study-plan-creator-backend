package pl.ioi.studyplancreator.domain.api.lecturer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Setter
public class DeleteLecturerCommand implements Command<Void> {
    private Long id;
}
