package pl.ioi.studyplancreator.domain.api.lecturer;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.Lecturer;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Getter
@Setter
@ToString
public class GetLecturerDetailsQuery implements Query<Lecturer> {
    private Long id;
}
