package pl.ioi.studyplancreator.domain.api.lecture.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
@ToString
public class UpdateLectureTypeCommand implements Command<Void> {
    private final LectureType lectureType;
}
