package pl.ioi.studyplancreator.domain.types.ga;

import lombok.*;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
public class GaLectureType implements Serializable {
    private long id;
    private String name;
    private Long twoWeeksAmount;
    private List<GaProfessor> professors;
    private List<GaRoomPurpose> neededRoomTypes;

    public GaProfessor randomProfessor() {
        var proffesorsArray = professors.toArray();
        return (GaProfessor) proffesorsArray[(int) (proffesorsArray.length * Math.random())];
    }
}
