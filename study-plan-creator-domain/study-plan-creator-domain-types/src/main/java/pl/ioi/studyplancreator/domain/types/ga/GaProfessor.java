package pl.ioi.studyplancreator.domain.types.ga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class GaProfessor implements Serializable {
    private Long id;
    private String name;
    private String surname;
}
