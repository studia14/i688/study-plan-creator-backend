package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.StringTokenizer;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Semester {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private String degree;

    private String number;

    @Column(nullable = false)
    @OneToMany
    @JoinColumn(name = "semester_id")
    private List<LectureType> lectureTypes;
}
