package pl.ioi.studyplancreator.domain.types.enums;

public enum Interval {
    EVERY_WEEK, EVEN_WEEK, ODD_WEEK
}
