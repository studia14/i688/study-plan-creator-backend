package pl.ioi.studyplancreator.domain.types.ga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GaGroup implements Serializable {
    private Long id;
    private List<GaLectureType> lectureTypes;

    public Collection<Long> getModuleIds() {
        return lectureTypes.stream()
                .map(GaLectureType::getId)
                .collect(Collectors.toList());
    }
}
