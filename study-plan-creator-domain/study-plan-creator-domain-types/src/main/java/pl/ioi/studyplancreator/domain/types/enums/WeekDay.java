package pl.ioi.studyplancreator.domain.types.enums;

public enum WeekDay {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
}
