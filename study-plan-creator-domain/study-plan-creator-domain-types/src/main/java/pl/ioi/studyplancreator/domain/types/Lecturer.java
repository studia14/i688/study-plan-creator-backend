package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Lecturer  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private String title;

    @ToString.Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "LECTURER_LECTURE_TYPES",
            joinColumns = @JoinColumn(name = "LECTURER"),
            inverseJoinColumns = @JoinColumn(name = "LECTURE_TYPE")
    )
    Set<LectureType> lectureTypes;
}
