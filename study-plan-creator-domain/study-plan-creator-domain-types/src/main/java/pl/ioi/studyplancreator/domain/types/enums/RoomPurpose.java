package pl.ioi.studyplancreator.domain.types.enums;

public enum RoomPurpose {
    INFORMATYCZNA, CHEMICZNA, ELEKTROTECHNICZNA, MATEMATYCZNA, POLONISTYCZNA, ZWYKŁA, IT
}
