package pl.ioi.studyplancreator.domain.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor(staticName = "of")
@Getter
public class DeansGroups {
    private List<DeansGroupDto> deansGroups;
}
