package pl.ioi.studyplancreator.domain.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.ioi.studyplancreator.domain.types.enums.WeekDay;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Timeslot {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="WEEKDAY", nullable = false)
    @Enumerated(EnumType.STRING)
    private WeekDay weekDay;

    @Column(nullable = false)
    private String time;
}
