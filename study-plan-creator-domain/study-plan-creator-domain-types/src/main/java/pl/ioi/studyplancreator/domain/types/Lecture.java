package pl.ioi.studyplancreator.domain.types;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.enums.Interval;
import pl.ioi.studyplancreator.domain.types.enums.WeekDay;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Lecture {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="lecturer_id", referencedColumnName = "id")
    private Lecturer lecturer;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Interval interval;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="timeslot_id", referencedColumnName = "id")
    private Timeslot timeslot;

    @OneToOne
    @JoinColumn(name="room_id", referencedColumnName = "id")
    private Room room;

    @OneToOne
    @JoinColumn(name="lecture_type_id", referencedColumnName = "id")
    private LectureType lectureType;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "LECTURE_DEANS_GROUP",
            joinColumns = @JoinColumn(name = "LECTURE"),
            inverseJoinColumns = @JoinColumn(name = "DEANS_GROUP")
    )
    Set<DeansGroup> deansGroups;
}

