package pl.ioi.studyplancreator.domain.types.ga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class GaRoom implements Serializable {
    private Long id;
    private List<GaRoomPurpose> purposes;
    private int capacity;
}
