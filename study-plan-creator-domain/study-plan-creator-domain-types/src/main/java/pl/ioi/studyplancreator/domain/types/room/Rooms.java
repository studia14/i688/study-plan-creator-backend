package pl.ioi.studyplancreator.domain.types.room;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.ioi.studyplancreator.domain.types.Room;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor(staticName = "of")
@ToString
public class Rooms {
    private List<Room> rooms;
}
