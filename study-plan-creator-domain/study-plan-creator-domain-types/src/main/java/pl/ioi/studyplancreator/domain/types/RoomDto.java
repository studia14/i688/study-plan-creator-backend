package pl.ioi.studyplancreator.domain.types;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@ToString
@Builder(toBuilder = true)
public class RoomDto {
    private String roomPurpose;
    private String number;
}
