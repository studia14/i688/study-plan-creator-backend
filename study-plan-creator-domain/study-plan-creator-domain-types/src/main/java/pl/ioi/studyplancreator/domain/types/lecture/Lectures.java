package pl.ioi.studyplancreator.domain.types.lecture;

import lombok.*;
import pl.ioi.studyplancreator.domain.types.Lecture;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor(staticName = "of")
@ToString
public class Lectures {
    private List<Lecture> lectures;
}
