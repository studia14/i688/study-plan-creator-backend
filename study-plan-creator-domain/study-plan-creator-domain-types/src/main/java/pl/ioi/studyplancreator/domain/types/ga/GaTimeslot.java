package pl.ioi.studyplancreator.domain.types.ga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.ioi.studyplancreator.domain.types.enums.WeekDay;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GaTimeslot implements Serializable {
    private long id;
    private WeekDay weekDay;
    private String time;
}
