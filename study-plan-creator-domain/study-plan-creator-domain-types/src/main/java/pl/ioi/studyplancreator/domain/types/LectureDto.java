package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class LectureDto {
    private String lecturer;
    private String classroom;
    private String year;
    private String interval; //TODO String czy typ?
    private String weekDay;
    private String time;
    private String name;
    private List<String> deansGroups;
}