package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LecturerDto {
    private String name;
    private String surname;
    private String title;
    private List<Long> lectureTypes;
}
