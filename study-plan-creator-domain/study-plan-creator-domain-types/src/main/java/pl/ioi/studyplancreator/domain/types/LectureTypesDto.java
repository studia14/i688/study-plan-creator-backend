package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Setter
@ToString
public class LectureTypesDto {
    private List<LectureType> lectureTypes;
}
