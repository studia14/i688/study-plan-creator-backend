package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Room  {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "ROOM_ROOM_PURPOSE",
            joinColumns = @JoinColumn(name = "ROOM_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROOM_PURPOSE_ID")
    )
    private Set<RoomPurpose> purposes;

    @Column(nullable = false)
    private String number;

    @Column(nullable = false)
    private int capacity;
}
