package pl.ioi.studyplancreator.domain.types.lecturer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.ioi.studyplancreator.domain.types.Lecturer;

import java.util.List;

@Getter
@AllArgsConstructor(staticName = "of")
public class Lecturers {
    private List<Lecturer> lecturers;
}
