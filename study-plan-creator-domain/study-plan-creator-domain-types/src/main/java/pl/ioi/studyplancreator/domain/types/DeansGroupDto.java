package pl.ioi.studyplancreator.domain.types;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class DeansGroupDto {
    private String id;
    private String name;
    private Long semesterId;
    private String semesterName;
}
