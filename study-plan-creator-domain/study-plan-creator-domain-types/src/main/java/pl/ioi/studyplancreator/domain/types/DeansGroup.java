package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeansGroup  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne()
    @JoinColumn(name = "semester_id")
    private Semester semester;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "LECTURE_DEANS_GROUP",
            joinColumns = @JoinColumn(name = "DEANS_GROUP"),
            inverseJoinColumns = @JoinColumn(name = "LECTURE")
    )
    private Set<Lecture> lectures;
}
