package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@ToString
@Builder(toBuilder = true)
public class SemesterDto {
    private String name;
    private String degree;
    private String number;
    private List<LectureType> lectureTypes;
}
