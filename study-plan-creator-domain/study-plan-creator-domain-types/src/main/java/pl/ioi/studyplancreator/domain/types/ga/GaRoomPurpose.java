package pl.ioi.studyplancreator.domain.types.ga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class GaRoomPurpose implements Serializable {
    private Long id;
    private String purpose;
}
