package pl.ioi.studyplancreator.domain.types;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LectureType  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String kind;

    @ManyToMany(mappedBy = "lectureTypes")
    private List<Lecturer> lecturers;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "LECTURE_TYPE_ROOM_PURPOSE",
            joinColumns = @JoinColumn(name = "LECTURE_TYPE_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROOM_PURPOSE_ID")
    )
    private Set<RoomPurpose> neededRoomTypes;
}
