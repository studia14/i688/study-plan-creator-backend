package pl.ioi.studyplancreator.standalone;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.application.repository.GeneratorJobLauncher;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class GeneratorJobLauncherImpl implements GeneratorJobLauncher {

    private final JobLauncher asyncJobLauncher;
    private final JobBuilderFactory jobBuilderFactory;
    private final Step prepareDataGeneticAlgorithmStep;
    private final Step processDataGeneticAlgorithmStep;
    private final Step writeDataGeneticAlgorithmStep;

    @Override
    public void start() {
        try {
            asyncJobLauncher.run(getJob(), new JobParameters());
        } catch (JobExecutionAlreadyRunningException | JobParametersInvalidException | JobInstanceAlreadyCompleteException | JobRestartException e) {
            e.printStackTrace();
        }
    }

    private Job getJob() {
        return jobBuilderFactory.get("generatePlanJob" + LocalDateTime.now())
                .start(prepareDataGeneticAlgorithmStep)
                .next(processDataGeneticAlgorithmStep)
                .next(writeDataGeneticAlgorithmStep)
                .build();
    }
}
