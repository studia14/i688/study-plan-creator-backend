package pl.ioi.studyplancreator.standalone;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import pl.ioi.studyplancreator.domain.application.ga.GaItemProcessor;
import pl.ioi.studyplancreator.domain.application.ga.GaItemReader;
import pl.ioi.studyplancreator.domain.application.ga.GaItemWriter;

@Configuration
@RequiredArgsConstructor
@EnableBatchProcessing
public class BatchConfiguration {

    private final StepBuilderFactory stepBuilderFactory;
    private final JobBuilderFactory jobBuilderFactory;
    private final JobRepository jobRepository;

    @Bean
    public JobLauncher asyncJobLauncher() {
        final SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        final SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
        jobLauncher.setTaskExecutor(simpleAsyncTaskExecutor);
        return jobLauncher;
    }

    @Bean
    public Step prepareDataGeneticAlgorithmStep(GaItemReader gaItemReader) {
        return stepBuilderFactory.get("prepareData")
                .tasklet(gaItemReader)
                .build();
    }

    @Bean
    public Step processDataGeneticAlgorithmStep(GaItemProcessor gaItemProcessor) {
        return stepBuilderFactory.get("processData")
                .tasklet(gaItemProcessor)
                .build();
    }

    @Bean
    public Step writeDataGeneticAlgorithmStep(GaItemWriter gaItemWriter) {
        return stepBuilderFactory.get("writeData")
                .tasklet(gaItemWriter)
                .build();
    }
}
