package pl.ioi.studyplancreator.standalone;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.common.utils.domain.context.Context;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.UserContext;

@Configuration
public class ContextConfiguration {

    @Bean
    public ContextHolder<UserContext> contextHolder() {
        return new ContextHolder<UserContext>() {
            @Override
            public void clearContext() {

            }

            @Override
            public UserContext getContext() {
                return null;
            }

            @Override
            public void setContext(UserContext context) {

            }
        };
    }
}
