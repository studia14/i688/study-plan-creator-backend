package pl.ioi.studyplancreator.standalone;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.AopConfiguration;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.CommandQueryHandlerConfiguration;

@SpringBootApplication
@Import({
        CommandQueryHandlerConfiguration.class,
        AopConfiguration.class
})
@Slf4j
@ComponentScan({
        "pl.ioi.studyplancreator"
})
@EnableJpaRepositories({
        "pl.ioi.studyplancreator.infrastructure.repository.jpa"
})
@EntityScan({
        "pl.ioi.studyplancreator.domain.types"
})
public class StudyPlanCreatorStandalone {
    public static void main(String[] args) {
        SpringApplication.run(StudyPlanCreatorStandalone.class, args);
    }

}
