package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.types.Lecture;

@Repository
public interface LectureJpaRepository extends JpaRepository<Lecture, Long> {
}
