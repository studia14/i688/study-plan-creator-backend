package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.application.repository.RoomRepository;
import pl.ioi.studyplancreator.domain.types.Room;
import pl.ioi.studyplancreator.domain.types.RoomDto;
import pl.ioi.studyplancreator.domain.types.room.Rooms;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.RoomJpaRepository;

import java.util.ArrayList;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class RoomRepositoryImpl implements RoomRepository {

    private final RoomJpaRepository roomJpaRepository;

//    @Override
//    public Room getRoomByNumber(String roomNumber) {
//        return roomJpaRepository.findAll()
//                .stream().filter(r -> r.getNumber().equals(roomNumber)).findFirst().orElse(null);
//    }

    @Override
    public Rooms getAll() {
        return Rooms.of(new ArrayList<>(roomJpaRepository.findAll()));
    }

    @Override
    public void save(RoomDto dto) {
        roomJpaRepository.save(Room.builder()
        //        .roomPurpose(dto.getRoomPurpose())
                .number(dto.getNumber())
                .build());
    }

    @Override
    public void update(RoomDto dto, Long id) {
        var existingRoom = roomJpaRepository.findById(id).orElseThrow();
        if (dto.getNumber() != null && !dto.getNumber().isBlank())
            existingRoom.setNumber(dto.getNumber());
     //   if (dto.getRoomPurpose() != null && !dto.getRoomPurpose().isEmpty())
/*
        if (dto.getRoomPurpose() != null && !dto.getRoomPurpose().isBlank())
            existingRoom.setRoomPurpose(dto.getRoomPurpose().replaceAll("\\s+", ""));
*/
        roomJpaRepository.save(existingRoom);
    }

    @Override
    public void deleteById(Long id) {
        roomJpaRepository.deleteById(id);
    }
}
