package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.application.repository.DeansGroupRepository;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.ioi.studyplancreator.domain.types.DeansGroup;
import pl.ioi.studyplancreator.domain.types.DeansGroupDto;
import pl.ioi.studyplancreator.domain.types.Semester;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.DeansGroupJpaRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class DeansGroupRepositoryImpl implements DeansGroupRepository {

    private final DeansGroupJpaRepository deansGroupJpaRepository;
    private final EntityManager entityManager;

    @Override
    public void save(DeansGroupDto deansGroup) {
        var semester = entityManager.getReference(Semester.class, deansGroup.getSemesterId());
        deansGroupJpaRepository.save(DeansGroup.builder()
                .name(deansGroup.getName())
                .semester(semester)
                .build());
    }

    @Override
    public void deleteById(Long id) {
        deansGroupJpaRepository.deleteById(id);
    }

    @Override
    public DeansGroup findById(Long id) {
        return deansGroupJpaRepository.findById(id).orElseThrow();
    }

    @Override
    public List<DeansGroup> findAll() {
        return deansGroupJpaRepository.findAll();
    }
}
