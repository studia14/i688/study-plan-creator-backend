package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ioi.studyplancreator.domain.types.Room;

public interface RoomsJpaRepository extends JpaRepository<Room, Long> {
}
