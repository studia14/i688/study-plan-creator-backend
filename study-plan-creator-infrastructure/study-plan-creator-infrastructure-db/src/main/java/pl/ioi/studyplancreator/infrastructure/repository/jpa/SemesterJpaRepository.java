package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ioi.studyplancreator.domain.types.Semester;

public interface SemesterJpaRepository extends JpaRepository<Semester, Long> {
}
