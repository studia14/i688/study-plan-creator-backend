package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.types.Lecturer;

import java.util.List;

@Repository
public interface LecturersJpaRepository extends JpaRepository<Lecturer, Long> {
    Lecturer findByNameOrSurname(String name, String surname);
}
