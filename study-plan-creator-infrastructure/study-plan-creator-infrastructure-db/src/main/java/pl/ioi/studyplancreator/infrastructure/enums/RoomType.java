package pl.ioi.studyplancreator.infrastructure.enums;

public enum RoomType {
    CWICZENIOWA, WYKLADOWA, LABORATORYJNA, SEMINARYJNA, ZWYKŁA
}
