package pl.ioi.studyplancreator.infrastructure.enums;

public enum UserRole {
    USER, ADMIN
}
