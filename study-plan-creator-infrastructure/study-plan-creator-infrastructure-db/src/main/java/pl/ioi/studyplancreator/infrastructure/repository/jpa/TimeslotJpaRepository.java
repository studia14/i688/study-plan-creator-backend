package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ioi.studyplancreator.domain.types.Timeslot;

public interface TimeslotJpaRepository extends JpaRepository<Timeslot, Long> {
}
