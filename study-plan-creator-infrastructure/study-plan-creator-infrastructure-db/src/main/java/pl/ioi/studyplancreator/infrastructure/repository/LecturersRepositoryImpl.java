package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.application.repository.LecturersRepository;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.ioi.studyplancreator.domain.types.Lecturer;
import pl.ioi.studyplancreator.domain.types.LecturerDto;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.LectureTypesJpaRepository;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.LecturersJpaRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class LecturersRepositoryImpl implements LecturersRepository {
    private final EntityManager entityManager;
    private final LecturersJpaRepository lecturersJpaRepository;

    @Override
    public List<Lecturer> findAll() {
        return lecturersJpaRepository.findAll();
    }

    @Override
    public Optional<Lecturer> getLecturerById(Long id) {
        return lecturersJpaRepository.findById(id);
    }

    @Override
    public void save(LecturerDto dto) {
        var lecturer = Lecturer.builder()
                .name(dto.getName())
                .surname(dto.getSurname())
                .title(dto.getTitle())
                .lectureTypes(getLectureTypeReferencesFromId(dto.getLectureTypes()))
                .build();
        lecturersJpaRepository.save(lecturer);
    }

    @Override
    public void update(LecturerDto lecturerDto, Long id) {
        var actualLecturer = lecturersJpaRepository.findById(id).orElseThrow();
        actualLecturer.setName(lecturerDto.getName());
        actualLecturer.setSurname(lecturerDto.getSurname());
        actualLecturer.setTitle(lecturerDto.getTitle());
        actualLecturer.setLectureTypes(getLectureTypeReferencesFromId(lecturerDto.getLectureTypes()));
        lecturersJpaRepository.save(actualLecturer);
    }

    @Override
    public void deleteById(Long id) {
        lecturersJpaRepository.deleteById(id);
    }

    private Set<LectureType> getLectureTypeReferencesFromId(List<Long> ids) {
        return ids.stream()
                .map(id -> entityManager.getReference(LectureType.class, id))
                .collect(Collectors.toSet());
    }
}
