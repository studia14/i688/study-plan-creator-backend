package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.application.repository.LectureTypesRepository;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.LectureTypesJpaRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class LectureTypesRepositoryImpl implements LectureTypesRepository {

    private final LectureTypesJpaRepository lectureTypesJpaRepository;

    @Override
    public List<LectureType> findAll() {
        return lectureTypesJpaRepository.findAll();
    }

    @Override
    public void save(LectureType lectureType) {
        lectureTypesJpaRepository.save(lectureType);
    }

    @Override
    public void update(LectureType lectureType) {
        var existingLectureType = lectureTypesJpaRepository.findById(lectureType.getId()).orElseThrow();
        existingLectureType.setName(lectureType.getName());
        lectureTypesJpaRepository.save(existingLectureType);
    }

    @Override
    public void deleteById(Long id) {
        lectureTypesJpaRepository.deleteById(id);
    }
}
