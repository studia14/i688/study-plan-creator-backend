package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.application.repository.RoomsRepository;
import pl.ioi.studyplancreator.domain.types.Room;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.RoomsJpaRepository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class RoomsRepositoryImpl implements RoomsRepository {

    private final RoomsJpaRepository roomsJpaRepository;

    @Override
    public List<Room> findAll() {
        return roomsJpaRepository.findAll();
    }
}
