package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.types.LectureType;

@Repository
public interface LectureTypesJpaRepository extends JpaRepository<LectureType, Long> {
}
