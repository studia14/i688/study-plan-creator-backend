package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.ioi.studyplancreator.domain.application.repository.SemesterRepository;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.ioi.studyplancreator.domain.types.Semester;
import pl.ioi.studyplancreator.domain.types.SemesterDto;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.LectureTypesJpaRepository;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.SemesterJpaRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class SemesterRepositoryImpl implements SemesterRepository {

    private final SemesterJpaRepository repository;
    private final LectureTypesJpaRepository lectureTypesJpaRepository;

    @Override
    public List<Semester> getSemesters() {
        return repository.findAll();
    }

    @Override
    public void save(SemesterDto dto) {
        repository.save(buildFromDto(dto));
    }

    @Override
    public void update(SemesterDto dto, Long id) {
        var existingSemester = repository.findById(id).orElseThrow();
        if(dto.getName() != null && !dto.getName().isBlank())
            existingSemester.setName(dto.getName());
        if(dto.getNumber() != null && !dto.getNumber().isBlank())
            existingSemester.setNumber(dto.getNumber());
        if(dto.getDegree() != null && !dto.getDegree().isBlank())
            existingSemester.setDegree(dto.getDegree());
        if(dto.getLectureTypes() != null && !dto.getLectureTypes().isEmpty())
            existingSemester.setLectureTypes(findLectureTypes(dto.getLectureTypes()));
        repository.save(existingSemester);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    private Semester buildFromDto(SemesterDto dto){
        return Semester.builder()
                .name(dto.getName())
                .degree(dto.getDegree())
                .number(dto.getNumber())
                .lectureTypes(findLectureTypes(dto.getLectureTypes()))
                .build();
    }

    //TODO dać CascadeType.PERSIST w modelu przy lectureTypes zamiast szukania streamem po findAll()
    private List<LectureType> findLectureTypes(List<LectureType> lectureTypes) {
        return lectureTypesJpaRepository.findAll()
                .stream()
                .filter(lt -> lectureTypes.contains(lt))
                .collect(Collectors.toList());
    }
}
