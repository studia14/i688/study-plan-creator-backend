package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.application.repository.TimeslotRepository;
import pl.ioi.studyplancreator.domain.types.Timeslot;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.TimeslotJpaRepository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TimeslotRepositoryImpl implements TimeslotRepository {

    private final TimeslotJpaRepository timeslotJpaRepository;

    @Override
    public List<Timeslot> findAll() {
        return timeslotJpaRepository.findAll();
    }
}
