package pl.ioi.studyplancreator.infrastructure.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.ioi.studyplancreator.domain.application.ga.FullLecture;
import pl.ioi.studyplancreator.domain.application.repository.LectureFilter;
import pl.ioi.studyplancreator.domain.application.repository.LecturesRepository;
import pl.ioi.studyplancreator.domain.types.*;
import pl.ioi.studyplancreator.domain.types.enums.Interval;
import pl.ioi.studyplancreator.domain.types.enums.WeekDay;
import pl.ioi.studyplancreator.infrastructure.repository.jpa.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class LecturesRepositoryImpl implements LecturesRepository {

    private final EntityManager entityManager;
    private final LectureJpaRepository lectureJpaRepository;
    private final LecturersJpaRepository lecturersJpaRepository;
    private final DeansGroupJpaRepository deansGroupJpaRepository;
    private final RoomJpaRepository roomRepository;
    private final LectureTypesJpaRepository lectureTypesJpaRepository;

    @Override
    public void save(LectureDto dto) {
        lectureJpaRepository.save(buildLectureFromDto(new Lecture(), dto));
    }

    @Override
    public void save(List<FullLecture> fullLectures) {
        var lectureEntities = fullLectures.stream()
                .map(fullLecture -> {
                    var lectureType = entityManager.getReference(LectureType.class, fullLecture.getModuleId());
                    var room = entityManager.getReference(Room.class, fullLecture.getRoomId());
                    var timeslot = entityManager.getReference(Timeslot.class, fullLecture.getTimeslotId());
                    var groups = fullLecture.getGroupIds().stream()
                            .map(groupId -> entityManager.getReference(DeansGroup.class, groupId))
                            .collect(Collectors.toSet());
                    var professor = entityManager.getReference(Lecturer.class, fullLecture.getProfessorId());
                    return Lecture.builder()
                            .lectureType(lectureType)
                            .lecturer(professor)
                            .deansGroups(groups)
                            .interval(Interval.EVERY_WEEK)
                            .room(room)
                            .timeslot(timeslot)
                            .build();
                })
                .collect(Collectors.toList());

        lectureJpaRepository.saveAll(lectureEntities);
    }

    @Override
    public void update(LectureDto dto, Long id) {
        lectureJpaRepository.save(buildLectureFromDto(
                lectureJpaRepository.findById(id).orElseThrow(),
                dto));
    }

    @Override
    public void deleteById(Long id) { lectureJpaRepository.deleteById(id); }

    @Override
    public void deleteAll() {
        lectureJpaRepository.deleteAll();
    }

    @Override
    public List<Lecture> findLecturesUsingFilter(LectureFilter lecturerFilter) {
        var criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Lecture> query = criteriaBuilder.createQuery(Lecture.class);
        Root<Lecture> rootQuery = query.from(Lecture.class);
        if (lecturerFilter.getPhrase() != null)
            return findLecturesUsingPhrase(lecturerFilter.getPhrase(), criteriaBuilder, rootQuery, query);

        Predicate predicate;
        if (lecturerFilter.getGroup() != null)
            return findLecturesUsingGroup(lecturerFilter.getGroup(), criteriaBuilder, rootQuery, query);
        else if (lecturerFilter.getLecturer() != null)
            predicate = predicateLecturesUsingLecturer(lecturerFilter.getLecturer(), criteriaBuilder, rootQuery);
        else if (lecturerFilter.getClassroom() != null)
            predicate = predicateLecturesUsingClassroom(lecturerFilter.getClassroom(), criteriaBuilder, rootQuery);
        else if (lecturerFilter.getYear() != null)
            return findLecturesUsingYear(lecturerFilter.getYear(), criteriaBuilder, rootQuery, query);
        else return lectureJpaRepository.findAll();

        query.select(rootQuery)
                .where(predicate);
        TypedQuery<Lecture> typedQuery = entityManager.createQuery(query);
        return typedQuery.getResultList();
    }

    private Lecture buildLectureFromDto(Lecture lecture, LectureDto dto){
        lecture = Lecture.builder()
                .lecturer(getLecturerByFullName(dto.getLecturer()))
                .interval(Interval.valueOf(dto.getInterval().toUpperCase()))
                .room(roomRepository.findAll().stream()
                        .filter(r -> r.getNumber().equals(dto.getClassroom())).findFirst().orElse(null))
                .deansGroups(deansGroupJpaRepository.findAll().stream()
                        .filter(g -> dto.getDeansGroups().contains(g.getName()))
                        .collect(Collectors.toSet()))
                .lectureType(lectureTypesJpaRepository.findAll().stream()
                        .filter(lt -> lt.getName().equals(dto.getName())).findFirst().orElse(null))
                .build();
        return lecture;
    }

    private Lecturer getLecturerByFullName(String fullName){
        String name[] = fullName.split(" ");
        Lecturer result = lecturersJpaRepository.findByNameOrSurname(name[0], name[1]);
        if(result != null)
            return result;
        return lecturersJpaRepository.findByNameOrSurname(name[1], name[0]);
    }

    private List<Lecture> findLecturesUsingGroup(String groupName, CriteriaBuilder criteriaBuilder,
                                                 Root<Lecture> rootQuery, CriteriaQuery<Lecture> query) {
        Join<Lecture, DeansGroup> groupJoin = rootQuery.join("deansGroups", JoinType.LEFT);
        query.select(rootQuery)
                .where(criteriaBuilder.equal(groupJoin.get("name"), groupName))
                .distinct(true);
        return entityManager.createQuery(query).getResultList();
    }

    private Predicate predicateLecturesUsingLecturer(String lecturerFullName, CriteriaBuilder criteriaBuilder,
                                                     Root<Lecture> rootQuery) {
        String[] lecturerSplitName = lecturerFullName.split(" ");
        if (lecturerSplitName.length == 2)
            return criteriaBuilder.and(
                    criteriaBuilder.equal(rootQuery.get("lecturer").<String>get("name"), lecturerSplitName[0]),
                    criteriaBuilder.equal(rootQuery.get("lecturer").<String>get("surname"), lecturerSplitName[1]));
        else return criteriaBuilder.equal(rootQuery.get("lecturer").<String>get("surname"), lecturerSplitName[0]);
    }

    private Predicate predicateLecturesUsingClassroom(String classroomName, CriteriaBuilder criteriaBuilder,
                                                      Root<Lecture> rootQuery) {
        return criteriaBuilder.equal(rootQuery.get("room").<String>get("number"), classroomName);
    }

    private List<Lecture> findLecturesUsingYear(String yearString, CriteriaBuilder criteriaBuilder,
                                                Root<Lecture> rootQuery, CriteriaQuery<Lecture> query) {
        Join<Lecture, DeansGroup> groupJoin = rootQuery.join("deansGroups", JoinType.LEFT);
        query.select(rootQuery)
                .where(criteriaBuilder.equal(groupJoin.get("studiesYear"), yearString))
                .distinct(true);
        return entityManager.createQuery(query).getResultList();
    }

    private List<Lecture> findLecturesUsingPhrase(String phrase, CriteriaBuilder criteriaBuilder,
                                                  Root<Lecture> rootQuery, CriteriaQuery<Lecture> query) {
        List<Lecture> result;
        Predicate predicate;

        String[] lecturerSplitName = phrase.split(" ");
        if (lecturerSplitName.length == 2)
            predicate = criteriaBuilder.and(
                    criteriaBuilder.like(rootQuery.get("lecturer").get("name"), lecturerSplitName[0]),
                    criteriaBuilder.like(rootQuery.get("lecturer").get("surname"), lecturerSplitName[1])
            );
        else predicate = criteriaBuilder.or(
                criteriaBuilder.like(rootQuery.get("lecturer").get("name"), lecturerSplitName[0]),
                criteriaBuilder.like(rootQuery.get("lecturer").get("surname"), lecturerSplitName[0])
        );
        query.select(rootQuery).where(predicate);
        result = entityManager.createQuery(query).getResultList();
        if (!result.isEmpty())
            return result;

        result = joinSelectWhere("name", phrase);
        if (!result.isEmpty())
            return result;

        predicate = criteriaBuilder.like(rootQuery.get("room").get("number"), phrase);
        query.select(rootQuery).where(predicate);
        result = entityManager.createQuery(query).getResultList();
        if (!result.isEmpty())
            return result;

        result = joinSelectWhere("studiesYear", phrase);
        if (!result.isEmpty())
            return result;

        return null;
    }

    private List<Lecture> joinSelectWhere(String column, String phrase) {
        var criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Lecture> query = criteriaBuilder.createQuery(Lecture.class);
        Root<Lecture> rootQuery = query.from(Lecture.class);
        Join<Lecture, DeansGroup> join = rootQuery.join("deansGroups", JoinType.LEFT);
        query.select(rootQuery)
                .where(criteriaBuilder.like(join.get(column), phrase))
                .distinct(true);
        return entityManager.createQuery(query).getResultList();
    }
}
