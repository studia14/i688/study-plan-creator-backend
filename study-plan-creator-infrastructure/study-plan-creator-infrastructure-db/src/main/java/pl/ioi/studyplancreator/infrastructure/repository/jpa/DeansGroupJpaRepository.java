package pl.ioi.studyplancreator.infrastructure.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.ioi.studyplancreator.domain.types.DeansGroup;

public interface DeansGroupJpaRepository extends JpaRepository<DeansGroup, Long> {
}
