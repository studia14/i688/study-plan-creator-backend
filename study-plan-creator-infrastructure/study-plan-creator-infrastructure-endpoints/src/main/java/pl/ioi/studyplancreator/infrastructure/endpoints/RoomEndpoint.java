package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.openapitools.api.RoomApi;
import org.openapitools.model.AddNewRoomRequest;
import org.openapitools.model.GetRoomsResponse;
import org.openapitools.model.UpdateRoomRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.room.AddNewRoomCommand;
import pl.ioi.studyplancreator.domain.api.room.DeleteRoomCommand;
import pl.ioi.studyplancreator.domain.api.room.GetRoomsQuery;
import pl.ioi.studyplancreator.domain.api.room.UpdateRoomCommand;
import pl.ioi.studyplancreator.domain.types.RoomDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class RoomEndpoint implements RoomApi {
    private final QueryExecutor queryExecutor;
    private final CommandExecutor commandExecutor;
    private final StudyPlanCreatorMapper studyPlanCreatorMapper;

    @Override
    public ResponseEntity<AddNewRoomRequest> addRoom(@Valid AddNewRoomRequest request) {
        commandExecutor.execute(AddNewRoomCommand.of(
                RoomDto.of(
                        strListToStr(request.getTypes()),
                        request.getName()
                )
        ));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteRoom(Integer id) {
        commandExecutor.execute(DeleteRoomCommand.of(Long.valueOf(id)));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<GetRoomsResponse> getRooms() {
        return ResponseEntity.ok(
                studyPlanCreatorMapper.getRoomsResponse(queryExecutor.execute(new GetRoomsQuery()))
        );
    }

    @Override
    public ResponseEntity<Void> updateRoom(Integer id, @Valid UpdateRoomRequest request) {
        commandExecutor.execute(UpdateRoomCommand.builder()
                .id(Long.valueOf(id))
                .room(RoomDto.builder()
                        .roomPurpose(strListToStr(request.getPurposes()))
                        .number(request.getName())
                        .build())
                .build());
        return ResponseEntity.ok().build();
    }

    private String strListToStr(List<String> listOfStrings) {
        return StringUtils.join(listOfStrings, ",");
    }
}
