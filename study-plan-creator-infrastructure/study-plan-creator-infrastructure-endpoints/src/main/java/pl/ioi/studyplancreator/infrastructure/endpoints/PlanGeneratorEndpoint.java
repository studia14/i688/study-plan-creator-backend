package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.GeneratorApi;
import org.openapitools.model.InitGeneratorStatusResponse;
import org.openapitools.model.PlanGeneratorStatusResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.generator.InitPlanGeneratorCommand;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class PlanGeneratorEndpoint implements GeneratorApi {

    private final CommandExecutor commandExecutor;

    @Override
    public ResponseEntity<PlanGeneratorStatusResponse> getGeneratorStatus(String name) {

        return GeneratorApi.super.getGeneratorStatus(name);
    }

    @Override
    public ResponseEntity<InitGeneratorStatusResponse> initPlanGenerator(@Valid Object body) {
        commandExecutor.execute(new InitPlanGeneratorCommand());
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
