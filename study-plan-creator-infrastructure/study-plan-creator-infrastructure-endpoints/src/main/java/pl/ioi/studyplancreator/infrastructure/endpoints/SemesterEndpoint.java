package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.SemesterApi;
import org.openapitools.model.AddSemesterRequest;
import org.openapitools.model.GetSemestersResponse;
import org.openapitools.model.UpdateSemesterRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.semester.AddNewSemesterCommand;
import pl.ioi.studyplancreator.domain.api.semester.DeleteSemesterCommand;
import pl.ioi.studyplancreator.domain.api.semester.GetSemestersQuery;
import pl.ioi.studyplancreator.domain.api.semester.UpdateSemesterCommand;
import pl.ioi.studyplancreator.domain.types.SemesterDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class SemesterEndpoint implements SemesterApi {

    private final QueryExecutor queryExecutor;
    private final CommandExecutor commandExecutor;
    private final StudyPlanCreatorMapper mapper;

    @Override
    public ResponseEntity<AddSemesterRequest> addSemester(@Valid AddSemesterRequest req) {
        commandExecutor.execute(AddNewSemesterCommand.of(
                SemesterDto.of(
                        req.getName(),
                        req.getDegree(),
                        req.getNumber(),
                        mapper.lectureType(req.getTypes())
                )
        ));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteSemester(Integer id) {
        commandExecutor.execute(DeleteSemesterCommand.of(Long.valueOf(id)));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<GetSemestersResponse> getSemesters() {
        return ResponseEntity.ok(mapper.toGetSemestersResponse(queryExecutor.execute(new GetSemestersQuery())));
    }

    @Override
    public ResponseEntity<Void> updateSemester(Integer id, @Valid UpdateSemesterRequest updateSemesterRequest) {
        commandExecutor.execute(UpdateSemesterCommand.of(
                Long.valueOf(id),
                mapper.updateSemesterDtoFromRequest(updateSemesterRequest)
        ));
        return ResponseEntity.ok().build();
    }
}
