package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.LectureTypesApi;
import org.openapitools.model.AddNewLectureTypeRequest;
import org.openapitools.model.LectureTypesResponse;
import org.openapitools.model.UpdateLectureTypeRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.lecture.type.AddNewLectureTypeCommand;
import pl.ioi.studyplancreator.domain.api.lecture.type.DeleteLectureTypeCommand;
import pl.ioi.studyplancreator.domain.api.lecture.type.FindLectureTypesQuery;
import pl.ioi.studyplancreator.domain.api.lecture.type.UpdateLectureTypeCommand;
import pl.ioi.studyplancreator.domain.types.LectureType;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class LectureTypesEndpoint implements LectureTypesApi {

    private final QueryExecutor queryExecutor;
    private final CommandExecutor commandExecutor;
    private final StudyPlanCreatorMapper studyPlanCreatorMapper;

    @Override
    public ResponseEntity<Void> addLectureType(@Valid AddNewLectureTypeRequest request) {
        commandExecutor.execute(AddNewLectureTypeCommand.of(request.getLectureType().getName()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteLectureType(Integer id) {
        commandExecutor.execute(DeleteLectureTypeCommand.of(Long.valueOf(id)));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<LectureTypesResponse> getLectureTypes() {
        return ResponseEntity.ok(
                studyPlanCreatorMapper.lectureTypesResponse(queryExecutor.execute(new FindLectureTypesQuery())));
    }

    @Override
    public ResponseEntity<Void> updateLectureType(Integer id, @Valid UpdateLectureTypeRequest request) {
        commandExecutor.execute(UpdateLectureTypeCommand.builder()
                .lectureType(LectureType.builder()
                        .id(Long.valueOf(id))
                        .name(request.getName())
                        .build())
                .build());
        return ResponseEntity.ok().build();
    }
}
