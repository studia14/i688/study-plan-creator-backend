package pl.ioi.studyplancreator.infrastructure.endpoints;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.openapitools.model.DeansGroup;
import org.openapitools.model.Lecture;
import org.openapitools.model.LectureType;
import org.openapitools.model.Lecturer;
import org.openapitools.model.Room;
import org.openapitools.model.*;
import pl.ioi.studyplancreator.domain.api.lecture.type.AddNewLectureTypeCommand;
import pl.ioi.studyplancreator.domain.api.lecturer.AddLecturerCommand;
import pl.ioi.studyplancreator.domain.types.*;
import pl.ioi.studyplancreator.domain.types.lecture.Lectures;
import pl.ioi.studyplancreator.domain.types.lecturer.Lecturers;
import pl.ioi.studyplancreator.domain.types.room.Rooms;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface StudyPlanCreatorMapper {

    GetLecturesResponse getLecturesResponse(Lectures input);
    GetRoomsResponse getRoomsResponse(Rooms input);

    @Mapping(target = "classroom", source = "room")
    @Mapping(target = "groups", source = "deansGroups")
    @Mapping(target = "dateInterval", source = "interval")
    @Mapping(target = "name", source = "lectureType.name")
    @Mapping(target = "type", source = "lectureType.kind")
    @Mapping(target = "hour", source = "timeslot.time")
    @Mapping(target = "dayOfWeek", source = "timeslot.weekDay")
    Lecture lecture(pl.ioi.studyplancreator.domain.types.Lecture input);

    DeansGroupDto deansGroupDto(DeansGroup input);

    DeansGroup deansGroup(DeansGroupDto input);

    GetDeansGroupsResponse getDeansGroupsResponse(DeansGroups input);

    SemesterDto updateSemesterDtoFromRequest(UpdateSemesterRequest request);

    Classroom classroom(Room input);

    List<pl.ioi.studyplancreator.domain.types.LectureType> lectureType(List<LectureType> lectureType);

    default List<String> map(String value){
        return Arrays.asList(value.split("\\s*,\\s*"));
    }

    @Mapping(target = "name", source = "number")
    org.openapitools.model.Room room(pl.ioi.studyplancreator.domain.types.Room input);

    @Mapping(target = "lecturer", source = "input")
    GetLecturerDetailsResponse getLecturerDetailsResponse(pl.ioi.studyplancreator.domain.types.Lecturer input);

    @Mapping(target = "lecturers", source = "lecturers")
    GetLecturersResponse getLecturersResponse(Lecturers input);

    @Mapping(target = "canTeach", source = "lectureTypes")
    Lecturer lecturer(pl.ioi.studyplancreator.domain.types.Lecturer input);

    AddLecturerCommand addNewLecturerCommand(AddNewLecturerRequest input);

    LecturerDto lecturerDto(Lecturer input);

    LecturerDto lecturerDto(LecturerData input);

    @AfterMapping
    default void lecturerDtoAfter(@MappingTarget LecturerDto lecturerDto, Lecturer input) {
        lecturerDto.setLectureTypes(input.getCanTeach().stream()
                .map(Activity::getId)
                .map(Long::parseLong)
                .collect(Collectors.toList()));
    }

    @AfterMapping
    default void lecturerDtoAfter(@MappingTarget LecturerDto lecturerDto, LecturerData input) {
        lecturerDto.setLectureTypes(input.getCanTeach().stream()
                .map(Activity::getId)
                .map(Long::parseLong)
                .collect(Collectors.toList()));
    }

    default GetSemestersResponse toGetSemestersResponse(List<pl.ioi.studyplancreator.domain.types.Semester> input) {
        return new GetSemestersResponse()
                .semesters(input
                        .stream()
                        .map(this::semester)
                        .collect(Collectors.toList())
                );
    }

    LectureTypesResponse lectureTypesResponse(LectureTypesDto input);
    org.openapitools.model.Semester semester(pl.ioi.studyplancreator.domain.types.Semester semester);
    //GetSemestersResponse semesterResponse(SemesterDto input);

    @Mapping(target = "name", source = "lectureType.name")
    AddNewLectureTypeCommand addNewLectureTypeCommand(AddNewLectureTypeRequest input);
}
