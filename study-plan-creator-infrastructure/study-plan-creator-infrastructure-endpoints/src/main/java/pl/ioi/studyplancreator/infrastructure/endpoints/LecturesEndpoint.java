package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.LecturesApi;
import org.openapitools.model.AddNewLectureRequest;
import org.openapitools.model.GetLecturesResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.lecture.AddLectureCommand;
import pl.ioi.studyplancreator.domain.api.lecture.FindLecturesQuery;
import pl.ioi.studyplancreator.domain.types.LectureDto;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class LecturesEndpoint implements LecturesApi {

    private final QueryExecutor queryExecutor;
    private final CommandExecutor commandExecutor;
    private final StudyPlanCreatorMapper studyPlanCreatorMapper;

    @Override
    public ResponseEntity<Void> addLecture(@Valid AddNewLectureRequest request) {
        LectureDto dto = LectureDto.builder()
                .lecturer(request.getLecturer())
                .classroom(request.getClassroom())
                .year(request.getYear())
                .interval(request.getInterval())
                .weekDay(request.getWeekDay())
                .time(request.getTime())
                .name(request.getName())
                .deansGroups(request.getDeansGroups())
                .build();
        commandExecutor.execute(new AddLectureCommand(dto));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<GetLecturesResponse> findLectures(@Valid String group, @Valid String lecturer, @Valid String classroom, @Valid String year, @Valid String phrase) {
        var query = FindLecturesQuery.of(group, lecturer, classroom, year, phrase);
        var result = queryExecutor.execute(query);
        return ResponseEntity.ok(studyPlanCreatorMapper.getLecturesResponse(result));
    }


}
