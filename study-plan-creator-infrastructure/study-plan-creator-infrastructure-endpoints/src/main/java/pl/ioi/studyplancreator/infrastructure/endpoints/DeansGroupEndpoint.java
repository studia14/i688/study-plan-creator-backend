package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.DeansGroupsApi;
import org.openapitools.model.AddNewDeansGroupRequest;
import org.openapitools.model.GetDeansGroupDetailsResponse;
import org.openapitools.model.GetDeansGroupsResponse;
import org.openapitools.model.UpdateDeansGroupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.deansgroup.*;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;
import java.math.BigDecimal;

@RequiredArgsConstructor
@RestController
public class DeansGroupEndpoint implements DeansGroupsApi {

    private final CommandExecutor commandExecutor;
    private final QueryExecutor queryExecutor;
    private final StudyPlanCreatorMapper studyPlanCreatorMapper;

    @Override
    public ResponseEntity<Void> addDeansGroup(@Valid AddNewDeansGroupRequest request) {
        commandExecutor.execute(AddDeansGroupCommand.of(studyPlanCreatorMapper.deansGroupDto(request.getDeansGroup())));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteDeansGroup(Integer id) {
        commandExecutor.execute(DeleteDeansGroupCommand.of((long)id));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<GetDeansGroupDetailsResponse> getDeansGroupDetails(Integer id) {
        var deansGroup = queryExecutor.execute(FindDeansGroupQuery.of((long)id));
        var response = new GetDeansGroupDetailsResponse();
        response.setDeansGroup(studyPlanCreatorMapper.deansGroup(deansGroup));;
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<GetDeansGroupsResponse> getDeansGroups() {
        var deansGroups = queryExecutor.execute(new FindDeansGroupsQuery());
        return ResponseEntity.ok(studyPlanCreatorMapper.getDeansGroupsResponse(deansGroups));
    }

    @Override
    public ResponseEntity<Void> updateDeansGroup(Integer id, @Valid UpdateDeansGroupRequest request) {
        var dto = studyPlanCreatorMapper.deansGroupDto(request.getDeansGroup());
        request.getDeansGroup().setId(BigDecimal.valueOf(id));
        commandExecutor.execute(UpdateDeansGroupCommand.of(dto));
        return ResponseEntity.ok().build();
    }
}
