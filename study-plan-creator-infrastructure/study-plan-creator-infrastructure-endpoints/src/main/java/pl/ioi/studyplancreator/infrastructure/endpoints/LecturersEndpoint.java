package pl.ioi.studyplancreator.infrastructure.endpoints;

import lombok.RequiredArgsConstructor;
import org.openapitools.api.LecturersApi;
import org.openapitools.model.AddNewLecturerRequest;
import org.openapitools.model.GetLecturerDetailsResponse;
import org.openapitools.model.GetLecturersResponse;
import org.openapitools.model.UpdateLecturerRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.ioi.studyplancreator.domain.api.lecturer.DeleteLecturerCommand;
import pl.ioi.studyplancreator.domain.api.lecturer.FindLecturersQuery;
import pl.ioi.studyplancreator.domain.api.lecturer.GetLecturerDetailsQuery;
import pl.ioi.studyplancreator.domain.api.lecturer.UpdateLecturerCommand;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
public class LecturersEndpoint implements LecturersApi {

    private final QueryExecutor queryExecutor;
    private final CommandExecutor commandExecutor;
    private final StudyPlanCreatorMapper studyPlanCreatorMapper;

    @Override
    public ResponseEntity<GetLecturerDetailsResponse> getLecturerDetails(Integer id) {
        return ResponseEntity.ok(studyPlanCreatorMapper.getLecturerDetailsResponse(queryExecutor.execute(GetLecturerDetailsQuery.of(Long.valueOf(id)))));
    }

    @Override
    public ResponseEntity<GetLecturersResponse> getLecturers() {
        return ResponseEntity.ok(studyPlanCreatorMapper.getLecturersResponse(queryExecutor.execute(new FindLecturersQuery())));
    }

    @Override
    public ResponseEntity<Void> addLecturer(@Valid AddNewLecturerRequest addNewLecturerRequest) {
        commandExecutor.execute(studyPlanCreatorMapper.addNewLecturerCommand(addNewLecturerRequest));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Void> updateLecturer(Integer id, @Valid UpdateLecturerRequest request) {
        commandExecutor.execute(UpdateLecturerCommand.of(Long.valueOf(id), studyPlanCreatorMapper.lecturerDto(request.getLecturer())));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteLecturer(Integer id) {
        commandExecutor.execute(DeleteLecturerCommand.of(Long.valueOf(id)));
        return ResponseEntity.ok().build();
    }
}
